# vitrubio.net web pages

repo for the [vitrubio.net](https://vitrubio.net) web site

## Hugo
Build using [Hugo](https://gohugo.io) and getting started https://gohugo.io/getting-started/usage/

### quick start
to start a clean new site `hugo new site NAMEOFTHESITE`

further on https://gohugo.io/getting-started/quick-start/

### run server
`hugo server`

run it with drafts
`hugo server -D`

### build static
`hugo -D` 
will output the website into `public/`

