---
draft: false
image: "/portfolio/arc-hive/arc-hive-zone-web-00.png"
showonlyimage: false
title: "arc-hive zone web"
date: 2021-10-01
weight: 0
tag: "web"
---

Develop and maitenance of the web sites [Arc-hive](https://arc-hive.zone) based on [WordPress](https://wordpress.org) and [Collections Arc-Hive](https://collections.arc-hive.zone) build on top of [OmekaS](omeka.org/) for [Hangar](https://hangar.org)

### general website:

![Arc-hive.zone web](arc-hive-zone-web-00.png)
![Arc-hive.zone web](arc-hive-zone-web-01.png)

### collections web site:

![Arc-hive.zone web](arc-hive-zone-collections-web-00.png)
![Arc-hive.zone web](arc-hive-zone-collections-web-01.png)
