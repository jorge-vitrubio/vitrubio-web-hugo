---
draft: false
image: "/portfolio/biofriction/biofriction-web-00.png"
showonlyimage: false
title: "biofriction web"
date: 2019-12-01
weight: 0
tag: "web"
---

Develop and maitenance of the web [Biofriction](https://biofriction.org) for [Hangar](https://hangar.org)

![Biofriction web](biofriction-web-00.png)
![Biofriction web](biofriction-web-01.png)
![Biofriction web](biofriction-web-02.png)
![Biofriction web](biofriction-web-03.png)
