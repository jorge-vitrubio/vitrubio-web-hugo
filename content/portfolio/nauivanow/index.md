---
draft: false
image: "/portfolio/nauivanow/nauivanow-02.jpg"
showonlyimage: false
title: "nau ivanow web"
date: "2016-11-01"
weight: 0
tag: "web"
---

Develop and maitenance of the 2015 web [NauIvanow](https://nauivanow.com)

Migration from Drupal6 to WordPress preserving the database history.

![naivannow web](nauivanow-01.jpg)
![naivannow web](nauivanow-02.jpg)
![naivannow web](nauivanow-03.jpg)
![naivannow web](nauivanow-04.jpg)
![naivannow web](nauivanow-05.jpg)
![naivannow web](nauivanow-06.jpg)
![naivannow web](nauivanow-07.jpg)
![naivannow web](nauivanow-08.jpg)
