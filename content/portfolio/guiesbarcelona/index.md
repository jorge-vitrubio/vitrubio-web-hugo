---
draft: false
image: "/portfolio/guiesbarcelona/guiesbarcelona-web-00.png"
showonlyimage: false
title: "guies barcelona web"
date: 2021-09-01
weight: 0
tag: "web"
---

Design, develop and maitenance of the web [Guies de Barcelona](https://guiesbarcelona.elglobusvermell.org) for [El Globus Vermell](https://elglobusvermell.org)

![Guies Barcelona web](guiesbarcelona-web-00.png)
![Guies Barcelona web](guiesbarcelona-web-01.png)
![Guies Barcelona web](guiesbarcelona-web-02.png)
![Guies Barcelona web](guiesbarcelona-web-03.png)
