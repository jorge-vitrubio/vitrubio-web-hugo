---
draft: false
image: "/portfolio/xrcb/xrcb-web-01.png"
showonlyimage: false
title: "xrcb web"
date: 2022-01-01
weight: 0
tag: "web"
---

CSS and HTML coding and development for [XRCB](https://xrcb.cat)

![xrcb web](xrcb-web-01.png)
![xrcb web](xrcb-web-02.png)
![xrcb web](xrcb-web-04.png)
![xrcb web](xrcb-web-06.png)
