---
draft: false
image: "portfolio/farawaysiria/farawaysiria-org-web-01.png"
showonlyimage: false
title: "FarawaySiria graphics, web, photograph"
date: 2013-10-01
weight: 0
tag: "web,graphic,photo"
---

Design, develop and maitenance of the 2013 exhibition's web [FarawaySiria](#)

![FarawaySiria web site](farawaysiria-org-web-00.png)
![FarawaySiria web site](farawaysiria-org-web-01.png)
