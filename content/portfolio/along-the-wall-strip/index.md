---
draft: false
image: "portfolio/along-the-wall-strip/along-the-wall-strip-book-00.jpg"
showonlyimage: false
title: "along the wall strip web and book"
date: 2019-08-01
weight: 0
tag: "web editorial"
---

Editoria design, preprinting and image photo retouching of the book and design, develop and maitenance of the web [Along the Wall Strip](https://elglobusvermell.org/along-the-wall-strip/) for [El Globus Vermell](https://elglobusvermell.org)

Book
![along the wall strip book](along-the-wall-strip-book-00.jpg)
![along the wall strip book](along-the-wall-strip-book-01.jpg)
![along the wall strip book](along-the-wall-strip-book-02.jpg)
![along the wall strip book](along-the-wall-strip-book-03.jpg)
![along the wall strip book](along-the-wall-strip-book-04.jpg)
![along the wall strip book](along-the-wall-strip-book-05.jpg)

Web site
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-00.jpg)
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-01.jpg)
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-02.jpg)
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-03.jpg)
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-04.jpg)
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-05.jpg)
![along the wall strip book](/portfolio/along-the-wall-strip/along-the-wall-strip-web-06.jpg)
