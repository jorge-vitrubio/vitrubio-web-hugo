---
draft: false
image: "/portfolio/patisxclima/patisxclima-web-2024-00.jpg"
showonlyimage: false
title: "Patis per clima web"
date: "2020-06-01"
weight: 0
tag: "web"
---

Design, develop and maitenance of the web [Patis per Clima](https://patisxclima.elglobusvermell.org) for [El Globus Vermell](https://elglobusvermell.org)

### 2024
![Patis per clima web 2024](patisxclima-web-2024-01.jpg)
![Patis per clima web 2024](patisxclima-web-2024-02.jpg)
![Patis per clima web 2024](patisxclima-web-2024-03.jpg)
![Patis per clima web 2024](patisxclima-web-2024-04.jpg)
![Patis per clima web 2024](patisxclima-web-2024-05.jpg)

### 2020
![Patis per clima web 2020](patisxclima-web-2020-00.png)
![Patis per clima web 2020](patisxclima-web-2020-01.png)
![Patis per clima web 2020](patisxclima-web-2020-02.png)
![Patis per clima web 2020](patisxclima-web-2020-03.png)
![Patis per clima web 2020](patisxclima-web-2020-04.png)
