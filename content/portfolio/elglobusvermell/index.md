---
draft: false
image: "/portfolio/elglobusvermell/elglobusvermell-grafica.jpg"
showonlyimage: false
title: "el globus vermell web logo"
date: 2015-07-01
weight: 0
tag: "web,grafic"
---

Design, develop and maitenance of the web [El Globus Vermell](https://elglobusvermell.org)

### Graphic design

![El globus vermell - grafica](elglobusvermell-grafica-constructiva.jpg)
![El globus vermell - grafica](elglobusvermell-grafica.jpg)
![El globus vermell - grafica](elglobusvermell-grafica-logotipos.jpg)

### Web design

![El globus vermell - grafica](elglobusvermell-blog.jpg)
![El globus vermell - grafica](elglobusvermell-homepage.jpg)
![El globus vermell - grafica](/portfolio/elglobusvermell/elglobusvermell-servei.jpg)
![El globus vermell - grafica](/portfolio/elglobusvermell/elglobusvermell-serveis.jpg)

