---
title: "About"
date: 2022-01-18
draft: false
---


![Jorge self portrait](2023-09-01-jorge-vitrubionet-square-v01.jpg)

My name is Jorge, I develop, design and produce web sites, I also maintain servers and IT infrastructure.

Originally specialized in editorial and print house jobs. I have been working since 1998 solving communication needs.

I enjoy photography very much, and I have comissiones some works.

Personally, love travel, cycloturing and bicycles.

Always learning, nowadays I'm into woodworking.

Just drop a line to info@vitrubio.net and we'll talk.

---

Me llamo Jorge, desarrollo diseño y produzco herramientas en la web, también mantengo servidores e infraestructura TIC.

En un origen me especialicé en diseño y producción editorial. Trabaje de manera principal en el este sector durante dos décadas. Desde 1998 vengo resolviendo necesidades en la comunicación.

Disfruto mucho de la fotografía, en algunas ocasiones he realizado trabajos profesionales.

Siempre aprendiendo, en la actualidad estoy con la ebanistería.

Simplemente escribe a info@vitrubio.net y hablaremos.

